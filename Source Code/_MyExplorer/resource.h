//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyExplorer.rc
//
#define IDC_MYICON                      2
#define IDD_MYEXPLORER_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_SMALL                       108
#define IDC_MYEXPLORER                  109
#define IDC_BACK                        113
#define IDC_NEXT                        114
#define IDC_UP                          115
#define IDC_TREEVIEW                    116
#define IDC_LISTVIEW                    117
#define IDR_MAINFRAME                   128
#define IDI_FILEEXPLORER                129
#define IDI_ICON1                       130
#define IDI_PC                          130
#define IDI_ICON2                       131
#define IDI_DESKTOP                     131
#define IDI_ICON3                       132
#define IDI_FLOPPY                      132
#define IDI_HDD                         133
#define IDI_CDROM                       134
#define IDI_USB                         135
#define IDI_FOLDER                      136
#define IDI_FILE                        137
#define ID_VIEW                         32771
#define ID_VIEW_MEDIUMICONS             32772
#define ID_VIEW_SMALLICONS              32773
#define ID_VIEW_LIST                    32774
#define ID_VIEW_DETAIL                  32775
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           118
#endif
#endif
