﻿#include "stdafx.h"
#include "MyExplorer.h"
#include "windowsx.h"
#include <commctrl.h>
#include <shlwapi.h>
#include <Shellapi.h>
#include <Shlobj.h> // using CSIDL

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "Shell32.lib")
#pragma comment(lib, "Comctl32.lib")
#pragma comment(lib, "shlwapi.lib")

#define MAX_LOADSTRING 100
#define MAX_PATH_LEN 10240		//Độ dài tối đa đường dẫn

#define iComputer 0
#define iDesktop 1
#define iFloppy 2
#define iHDD 3
#define iCD 4
#define iUSB 5
#define iFolder 6
#define iFile 7

// Biến toàn cục ---------------------------------------------------
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
struct DriveInfo
{
	TCHAR drive[4];
	TCHAR volume[30];
	TCHAR label[35];
	int icon;
};

typedef struct fPath * path_node;
struct fPath
{
	TCHAR path[MAX_PATH_LEN];
	path_node prev;
	path_node next;
};

path_node url;
TCHAR desktopPath[MAX_PATH_LEN];
DriveInfo *iDrive;
int g_DriveCount = 0;
HWND g_ListView, g_TreeView, g_Address, g_btBack, g_btNext, g_btUp;
bool g_bStarted;
HTREEITEM hMyPC, hDesktop, hQuickAccess;
// -----------------------------------------------------------------

// Khai báo hàm ----------------------------------------------------
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnDestroy(HWND hWnd);

void OnCreate_Button(HWND hWnd);
void OnCreate_ImageList();
HFONT OnCreate_GetSystemFont(int dSize = 0);
DriveInfo* OnCreate_GetDriveInfo(int &g_DriveCount);

LPCWSTR TV_GetPath(HTREEITEM hItem);
void TV_PreLoad(HTREEITEM hItem);
void TV_LoadPC();
void TV_LoadChild(HTREEITEM &hParent, LPCWSTR path, BOOL bShowHiddenSystem = FALSE);
void LV_LoadPC();
void LV_LoadChild(LPCWSTR path);
void LV_ChangeView(int nNewStyle);
LPWSTR LV_GetFileSize(__int64 nSize);
LPWSTR LV_GetDateModified(const FILETIME &ftLastWrite);

void Node_Add(path_node &Current, LPCWSTR path);
path_node Node_Back(path_node p);
path_node Node_Next(path_node p);
LPCWSTR Node_GetPath(path_node p);
void Node_Destroy(path_node &head);
// -----------------------------------------------------------------

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MYEXPLORER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYEXPLORER));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FILEEXPLORER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYEXPLORER);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_FILEEXPLORER));

	return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int mWidth, mHeight;
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_SIZE:
		RECT main;
		GetWindowRect(hWnd, &main);
		mWidth = main.right - main.left;
		mHeight = main.bottom - main.top;

		MoveWindow(g_Address, 150, 5, mWidth - 150 - 21, 20, true);
		MoveWindow(g_TreeView, 5, 30, 190, mHeight - 95, true);
		MoveWindow(g_ListView, 200, 30, mWidth - 221, mHeight - 95, true);
		ListView_Arrange(g_ListView, LVA_ALIGNTOP);
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

HFONT OnCreate_GetSystemFont(int dSize)
{
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight - dSize, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	return hFont;
}

DriveInfo* OnCreate_GetDriveInfo(int &g_DriveCount)
{
	TCHAR buffer[105]; //Để chữa chuỗi tối đa 26 x 4 + 1 kí tự (Ví dụ "A:\")
	g_DriveCount = 0;
	int i, j, k;

	GetLogicalDriveStrings(105, buffer);

	//Đếm số lượng ổ đĩa 
	for (i = 0; !((buffer[i] == 0) && (buffer[i + 1] == 0)); ++i)
		if (buffer[i] == 0)
			++g_DriveCount;
	++g_DriveCount;

	DriveInfo *iDrive = NULL;
	iDrive = new DriveInfo[g_DriveCount];

	i = 0;
	for (j = 0; j < g_DriveCount; j++)
	{
		k = 0;
		while (buffer[i] != 0)
			iDrive[j].drive[k++] = buffer[i++];
		iDrive[j].drive[k] = NULL; //Kết thúc chuỗi
		++i;
	}
	for (i = 0; i < g_DriveCount; i++)
	{
		StrCpy(buffer, _T(""));
		int type = GetDriveType(iDrive[i].drive);
		GetVolumeInformation(iDrive[i].drive, buffer, 105, NULL, NULL, NULL, NULL, 0);

		// Lấy icon ổ đĩa
		switch (type)
		{
		case DRIVE_REMOVABLE:
			if (i > 1)
				iDrive[i].icon = iUSB; //Dùng tạm icon đĩa mềm
			else
				iDrive[i].icon = iFloppy;
			break;
		case DRIVE_FIXED:
			iDrive[i].icon = iHDD;
			break;
		case DRIVE_CDROM:
			iDrive[i].icon = iCD;
			break;
		default:
			iDrive[i].icon = iFloppy; //Dùng tạm icon đĩa mềm
		}

		// Lấy tên ổ đĩa
		if (wcslen(buffer) == 0) {
			switch (type)
			{
			case DRIVE_REMOVABLE:
				if (i > 1)
					StrCpy(iDrive[i].volume, _T("Removable"));
				else
					StrCpy(iDrive[i].volume, _T("3½ Floppy"));
				break;
			case DRIVE_FIXED:
				StrCpy(iDrive[i].volume, _T("Local Disk"));
				break;
			case DRIVE_CDROM:
				StrCpy(iDrive[i].volume, _T("CD-ROM"));
				break;
			default:
				StrCpy(iDrive[i].volume, _T("Unknown"));
			}
		}
		else {
			StrCpy(iDrive[i].volume, buffer);
		}

		// Nhãn ổ đĩa, vd WIN (C:)
		StrCpy(iDrive[i].label, iDrive[i].volume);
		StrCat(iDrive[i].label, _T(" ("));
		StrNCat(iDrive[i].label, iDrive[i].drive, 3);
		StrCat(iDrive[i].label, _T(")"));
	}
	return iDrive;
}

void OnCreate_Button(HWND hWnd)
{
	HFONT hFontS = OnCreate_GetSystemFont(2);
	HFONT hFontL = OnCreate_GetSystemFont(6);
	//Adress + Button
	g_Address = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL,
		5, 5, 500, 25, hWnd, NULL, hInst, NULL);
	SendMessage(g_Address, WM_SETFONT, WPARAM(hFontS), TRUE);

	g_btBack = CreateWindow(L"BUTTON", L"«", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		5, 5, 50, 20, hWnd, (HMENU)IDC_BACK, hInst, NULL);
	SendMessage(g_btBack, WM_SETFONT, WPARAM(hFontL), TRUE);

	g_btNext = CreateWindow(L"BUTTON", L"»", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		60, 5, 50, 20, hWnd, (HMENU)IDC_NEXT, hInst, NULL);
	SendMessage(g_btNext, WM_SETFONT, WPARAM(hFontL), TRUE);

	g_btUp = CreateWindow(L"BUTTON", L"˄", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		115, 5, 30, 20, hWnd, (HMENU)IDC_UP, hInst, NULL);
	SendMessage(g_btUp, WM_SETFONT, WPARAM(hFontS), TRUE);
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Khởi nạo node HEAD
	url = new fPath;
	url->prev = NULL;
	url->next = NULL;
	StrCpy(url->path, _T(""));

	OnCreate_Button(hWnd);

	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	// Lấy thông tin ổ đĩa
	iDrive = OnCreate_GetDriveInfo(g_DriveCount);

	// Lấy địa chỉ folder Desktop
	SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, SHGFP_TYPE_DEFAULT, desktopPath);

	//Tree view
	g_TreeView = CreateWindow(WC_TREEVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT | WS_BORDER,
		5, 50, 185, 300, hWnd, (HMENU)IDC_TREEVIEW, hInst, NULL);
	g_ListView = CreateWindow(WC_LISTVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER,
		200, 50, 300, 200, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);
	OnCreate_ImageList();

	TV_LoadPC();

	TreeView_SelectItem(g_TreeView, hMyPC);
	SetWindowText(g_Address, TV_GetPath(hMyPC));
	TreeView_Expand(g_TreeView, hMyPC, TVE_EXPAND);

	//List view
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Name";
	lvCol.cx = 200;
	ListView_InsertColumn(g_ListView, 0, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Date Modified";
	lvCol.cx = 120;
	ListView_InsertColumn(g_ListView, 1, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Type";
	lvCol.cx = 100;
	ListView_InsertColumn(g_ListView, 2, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_WIDTH;
	lvCol.pszText = L"Size";
	lvCol.cx = 70;
	ListView_InsertColumn(g_ListView, 3, &lvCol);

	LV_LoadPC();
	LV_ChangeView(LVS_ICON);

	g_bStarted = true;

	return true;
}

LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm)
{
	if (g_bStarted)
	{
		LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;
		HTREEITEM hCurrent = lpnmTree->itemNew.hItem;

		TCHAR buffer[MAX_PATH_LEN], url_path[MAX_PATH_LEN];

		switch (pnm->code)
		{
		case TVN_ITEMEXPANDING:
			if (hCurrent != hMyPC)
			{
				HTREEITEM hChild = TreeView_GetNextItem(g_TreeView, hCurrent, TVGN_CHILD);
				if (!StrCmp(TV_GetPath(hChild), _T("PreLoad")))
				{
					TreeView_DeleteItem(g_TreeView, hChild);
					TV_LoadChild(hCurrent, TV_GetPath(hCurrent));
				}
			}
			break;
		case TVN_SELCHANGED:
			if (hCurrent == hMyPC)
				StrCpy(buffer, _T(""));
			else
				StrCpy(buffer, TV_GetPath(hCurrent));
			StrCpy(url_path, buffer);
			ListView_DeleteAllItems(g_ListView);
			LV_LoadChild(buffer);
			Node_Add(url, url_path);
			if (wcslen(buffer) == 0)
				StrCpy(buffer, _T("This PC"));
			SetWindowText(g_Address, buffer);
			break;
		//case NM_CLICK:
		//	if (pnm->hwndFrom == g_TreeView)
		//	{
		//		if (hCurrent == hMyPC)
		//			StrCpy(buffer, _T(""));
		//		StrCpy(url_path, buffer);
		//		ListView_DeleteAllItems(g_ListView);
		//		LV_LoadChild(buffer);
		//		Node_Add(url, url_path);
		//		if (wcslen(buffer) == 0)
		//			StrCpy(buffer, _T("This PC"));
		//		SetWindowText(g_Address, buffer);
		//	}
		//	break;
		case NM_DBLCLK:
			if (pnm->hwndFrom == g_ListView)
			{
				int iPos = ListView_GetNextItem(g_ListView, -1, LVNI_SELECTED);
				LVITEM item;
				while (iPos != -1) {					
					item.mask = LVIF_PARAM | LVIF_IMAGE;
					item.iItem = iPos;
					item.iSubItem = 0;

					ListView_GetItem(g_ListView, &item);
					StrCpy(buffer, (LPCWSTR)item.lParam);

					iPos = -1; // Chỉ thao tác với file đầu tiên được d_click rồi thoát bòng lặp
				}
				if (item.iImage != iFile) // Chỉ mở đường dẫn nếu không phải là tập tin
				{
					ListView_DeleteAllItems(g_ListView);
					LV_LoadChild(buffer);
					Node_Add(url, buffer);
					SetWindowText(g_Address, buffer);
				}
			}
			break;
		}
	}
	return 0;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	TCHAR buffer[MAX_PATH_LEN];
	switch (id)
	{
	case IDC_BACK: // button "BACK"
		if (url != NULL)
		{
			if (Node_Back(url) == NULL)
				break;
			else
			{
				url = Node_Back(url);
				StrCpy(buffer, Node_GetPath(url));
				ListView_DeleteAllItems(g_ListView);
				LV_LoadChild(buffer);
				if (wcslen(buffer) == 0)
					StrCpy(buffer, _T("This PC"));
				SetWindowText(g_Address, buffer);
			}
		}
		break;
	case IDC_NEXT: // button "NEXT"
		if (url != NULL)
		{
			if (Node_Next(url) == NULL)
				break;
			else
			{
				url = Node_Next(url);
				StrCpy(buffer, Node_GetPath(url));
				ListView_DeleteAllItems(g_ListView);
				LV_LoadChild(buffer);
				if (wcslen(buffer) == 0)
					StrCpy(buffer, _T("This PC"));
				SetWindowText(g_Address, buffer);
			}
		}
		break;
	case IDC_UP: // button "UP"
		if (url != NULL)
		{
			StrCpy(buffer, Node_GetPath(url));
			if (wcslen(buffer) == 0) // đang trỏ tới This PC
				break;
			else if (wcslen(buffer) == 3) // đang trỏ tới ổ đĩa, vd "D:\"
				StrCpy(buffer, _T(""));
			else // đang trỏ tới thư mục "D:\Video"
			{
				int i = wcslen(buffer) - 1;
				while (i)
				{
					if (buffer[i] == L'\\')
					{
						buffer[i] = NULL;
						break;
					}
					i--;
				}
			}
			if (wcslen(buffer) == 2)
				StrCat(buffer, _T("\\"));
			ListView_DeleteAllItems(g_ListView);
			LV_LoadChild(buffer);
			Node_Add(url, buffer);
			if (wcslen(buffer) == 0)
				StrCpy(buffer, _T("This PC"));
			SetWindowText(g_Address, buffer);
		}
		break;
	case ID_VIEW_MEDIUMICONS:
		LV_ChangeView(LVS_ICON);
		break;
	case ID_VIEW_SMALLICONS:
		LV_ChangeView(LVS_SMALLICON);
		break;
	case ID_VIEW_LIST:
		LV_ChangeView(LVS_LIST);
		break;
	case ID_VIEW_DETAIL:
		LV_ChangeView(LVS_REPORT);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnDestroy(HWND hWnd)
{
	delete[] iDrive;
	Node_Destroy(url);
	PostQuitMessage(0);
}

LPCWSTR TV_GetPath(HTREEITEM hItem)
{
	TVITEMEX tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	TreeView_GetItem(g_TreeView, &tv);
	return (LPCWSTR)tv.lParam;
}

void TV_PreLoad(HTREEITEM hItem)
{
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, TV_GetPath(hItem));

	if (wcslen(buffer) == 3) //Nếu quét các ổ đĩa
	{
		if (StrStr(buffer, _T("A:")) || StrStr(buffer, _T("B:"))) //Không quét đĩa mềm
			return;
	}
	else
		StrCat(buffer, _T("\\"));

	StrCat(buffer, _T("*"));

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);

	if (hFile == INVALID_HANDLE_VALUE)
		return;

	BOOL bFound = true;

	//Trong khi còn tìm thấy file hay thư mục
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			TV_INSERTSTRUCT tvInsert;
			tvInsert.hParent = hItem;
			tvInsert.hInsertAfter = TVI_LAST;
			tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
			tvInsert.item.pszText = _T("PreLoad");
			tvInsert.item.lParam = (LPARAM)_T("PreLoad");
			TreeView_InsertItem(g_TreeView, &tvInsert);
			bFound = FALSE;
		}
		else
			bFound = FindNextFileW(hFile, &fd);
	}
}

void TV_LoadPC()
{
	TV_INSERTSTRUCT tvInsert;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	tvInsert.hParent = NULL;
	tvInsert.item.iImage = iComputer;
	tvInsert.item.iSelectedImage = iComputer;
	tvInsert.item.pszText = _T("This PC");
	tvInsert.item.lParam = (LPARAM)_T("This PC");
	hMyPC = TreeView_InsertItem(g_TreeView, &tvInsert);

	tvInsert.hParent = hMyPC;
	tvInsert.item.iImage = iDesktop;
	tvInsert.item.iSelectedImage = iDesktop;
	tvInsert.item.pszText = _T("Desktop");
	tvInsert.item.lParam = (LPARAM)desktopPath;
	hDesktop = TreeView_InsertItem(g_TreeView, &tvInsert);
	TV_PreLoad(hDesktop);

	HTREEITEM *hDrive = new HTREEITEM[g_DriveCount];
	for (int i = 0; i < g_DriveCount; i++) {
		tvInsert.hParent = hMyPC;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.iImage = iDrive[i].icon;
		tvInsert.item.iSelectedImage = iDrive[i].icon;
		tvInsert.item.pszText = iDrive[i].label;
		tvInsert.item.lParam = (LPARAM)iDrive[i].drive;
		hDrive[i] = TreeView_InsertItem(g_TreeView, &tvInsert);
		TV_PreLoad(hDrive[i]);
	}
}

void TV_LoadChild(HTREEITEM &hParent, LPCWSTR path, BOOL bShowHiddenSystem)
{
	TCHAR buffer[MAX_PATH_LEN];
	StrCpy(buffer, path);

	StrCat(buffer, _T("\\*"));

	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);
	BOOL bFound = 1;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	TCHAR * folderPath;
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			tvInsert.item.pszText = fd.cFileName;
			tvInsert.item.iImage = iFolder;
			tvInsert.item.iSelectedImage = iFolder;
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];

			StrCpy(folderPath, path);
			if (wcslen(path) != 3)
				StrCat(folderPath, _T("\\"));
			StrCat(folderPath, fd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;
			HTREEITEM hItem = TreeView_InsertItem(g_TreeView, &tvInsert);
			//Preload
			TV_PreLoad(hItem);
		}
		bFound = FindNextFileW(hFile, &fd);
	}
}

void LV_LoadPC()
{
	LV_ITEM lv;
	for (int i = 0; i < g_DriveCount; i++)
	{
		//Nạp cột đầu tiên cũng là thông tin chính
		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv.iItem = i;
		lv.iImage = iDrive[i].icon;

		lv.iSubItem = 0;
		lv.pszText = iDrive[i].label;
		lv.lParam = (LPARAM)iDrive[i].drive;
		ListView_InsertItem(g_ListView, &lv);
	}
}

void LV_LoadChild(LPCWSTR path)
{
	if (wcslen(path) < 3)
		LV_LoadPC();
	else
	{
		TCHAR buffer[MAX_PATH_LEN];
		StrCpy(buffer, path);

		if (wcslen(path) == 3) //Nếu quét các ổ đĩa
			StrCat(buffer, _T("*"));
		else
			StrCat(buffer, _T("\\*"));

		WIN32_FIND_DATA fd;
		BOOL bFound = true;
		HANDLE hFile;
		LV_ITEM lv;

		TCHAR * folderPath;
		int nItemCount = 0;

		hFile = FindFirstFileW(buffer, &fd);

		if (hFile == INVALID_HANDLE_VALUE)
			bFound = FALSE;

		while (bFound)
		{
			if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
				((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
				(StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
			{
				folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
				StrCpy(folderPath, path);

				if (wcslen(path) != 3)
					StrCat(folderPath, _T("\\"));

				StrCat(folderPath, fd.cFileName);

				lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
				lv.iItem = nItemCount;
				lv.iSubItem = 0;
				lv.pszText = fd.cFileName;
				lv.iImage = iFolder;
				lv.lParam = (LPARAM)folderPath;
				ListView_InsertItem(g_ListView, &lv);

				//Date modified
				ListView_SetItemText(g_ListView, nItemCount, 1, LV_GetDateModified(fd.ftLastWriteTime));

				//Type
				ListView_SetItemText(g_ListView, nItemCount, 2, _T("File Folder"));
				
				nItemCount++;
			}
			bFound = FindNextFileW(hFile, &fd);
		}

		TCHAR *filePath;

		hFile = FindFirstFileW(buffer, &fd);
		bFound = TRUE;

		if (hFile == INVALID_HANDLE_VALUE)
			bFound = FALSE;

		while (bFound)
		{
			if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
				((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
				((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
			{
				filePath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
				StrCpy(filePath, path);

				if (wcslen(path) != 3)
					StrCat(filePath, _T("\\"));

				StrCat(filePath, fd.cFileName);

				//Cột thứ nhất là tên hiển thị của tập tin
				lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
				lv.iItem = nItemCount;
				lv.iSubItem = 0;
				lv.iImage = iFile;
				lv.pszText = fd.cFileName;
				lv.lParam = (LPARAM)filePath;
				ListView_InsertItem(g_ListView, &lv);

				//Date Modified	
				ListView_SetItemText(g_ListView, nItemCount, 1, LV_GetDateModified(fd.ftLastWriteTime));

				//Type
				//ListView_SetItemText(g_ListView, nItemCount, 2, GetFileType(fd));

				//Size
				ListView_SetItemText(g_ListView, nItemCount, 3, LV_GetFileSize(fd.nFileSizeLow));

				nItemCount++;
			}
			bFound = FindNextFileW(hFile, &fd);
		}
	}
}

void OnCreate_ImageList()
{
	HIMAGELIST *hLarge = new HIMAGELIST;
	HIMAGELIST *hSmall = new HIMAGELIST;

	*hSmall = ImageList_Create(16, 16, ILC_COLOR32, 8, 65535);
	*hLarge = ImageList_Create(32, 32, ILC_COLOR32, 8, 65535);

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_PC)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_PC)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_DESKTOP)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_DESKTOP)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FLOPPY)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FLOPPY)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_HDD)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_HDD)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_CDROM)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_CDROM)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_USB)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_USB)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FOLDER)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FOLDER)));

	ImageList_AddIcon(*hLarge, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FILE)));
	ImageList_AddIcon(*hSmall, LoadIcon(hInst, MAKEINTRESOURCE(IDI_FILE)));

	TreeView_SetImageList(g_TreeView, *hSmall, TVSIL_NORMAL);

	ListView_SetImageList(g_ListView, *hSmall, LVSIL_SMALL);
	ListView_SetImageList(g_ListView, *hLarge, LVSIL_NORMAL);

	delete[] hLarge;
	delete[] hSmall;
}

void Node_Add(path_node &Current, LPCWSTR path)
{
	path_node p = new fPath;
	StrCpy(p->path, path);
	Current->next = p;
	p->prev = Current;
	p->next = NULL;
	Current = p;
}

path_node Node_Back(path_node p)
{
	return p->prev;
}

path_node Node_Next(path_node p)
{
	return p->next;
}

LPCWSTR Node_GetPath(path_node p)
{
	return p->path;
}

void Node_Destroy(path_node &head)
{
	path_node q = head->next;
	while (q)
	{
		delete head;
		head = q;
	}
}

LPWSTR LV_GetFileSize(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, 10);

	if (nRight != 0 && nType > 1)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, 10);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" bytes"));
		break;
	case 1:
		StrCat(buffer, _T(" KB"));
		break;
	case 2:
		StrCat(buffer, _T(" MB"));
		break;
	case 3:
		StrCat(buffer, _T(" GB"));
		break;
	case 4:
		StrCat(buffer, _T(" TB"));
		break;
	}
	return buffer;
}

LPWSTR LV_GetDateModified(const FILETIME &ftLastWrite)
{
	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d %s"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour>12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour>12) ? (_T("PM")) : (_T("AM")));

	return buffer;
}

void LV_ChangeView(int nNewStyle)
{
	LONG dNotView = ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
	SetWindowLong(g_ListView, GWL_STYLE, GetWindowLong(g_ListView, GWL_STYLE) & dNotView | nNewStyle);
}