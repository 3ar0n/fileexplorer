## Thông tin cá nhân
Họ và tên: **Đặng Quốc Thái**
MSSV: **1512501**

## Các chức năng
* ListView (bên phải) và TreeView (bên trái) để duyệt ổ đĩa và thư mục
* Hiển thị toàn bộ thư mục và tập tin tương ứng với 1 đường dẫn trong ListView (click trong TreeView hoặc double click trong ListView)
* List view có 4 cột: Tên, Loại (chỉ với thư mục), Thời gian chỉnh sửa, Dung lượng (chỉ với tập tin)
* Cho phép thay đổi cách hiển thị ListView (Medium icons, Small icons, List, Detail)
* Set cứng icon (Computer, Desktop, các loại ổ đĩa, Folder, File)
* Cho phép quay lui (back), tiến (next), về thư mục cha (up) đối với đường dẫn thư mục
* Không hỗ trợ thay đổi kích thước TreeView, ListView, chỉ tự động điều chỉnh kích thước khi thay đổi kích thước cửa sổ chính
* Không chạy ứng dụng tương ứng khi bấm mở tập tin
* Không xử lí rename, copy, delete, kéo thả
* Không hỗ trợ các phím tắt như F2, Del, Ctrl + A, Cut, Copy, Move.

## Luồng sự kiện chính
1. Chạy chương trình lên, hiển thị node This PC trên TreeView ở trạng thái expand (mở rộng), load sẵn các node con là các ổ đĩa và thư mục Desktop. Đồng thời hiển thị các ổ đĩa trong ListView (dưới dạng Medium icons)
2. Bấm vào nút expand (+) (ổ đĩa, Desktop, thư mục) đang ở trạng thái collapse(thu gọn) trong TreeView sẽ xổ xuống danh sách các thư mục con.
3. Bấm vào vào ổ đĩa, thư mục (ở trạng thái collapse/expand) trong TreeView sẽ hiển thị toàn bộ thư mục, và tập tin nằm trong thư mục đó trong ListView.
4. Bấm đôi vào thư mục trong ListView sẽ hiển thị toàn bộ thư mục, và tập tin nằm trong thư mục đó trong ListView.

## Luồng sự kiện phụ
1. Thanh địa chỉ (address bar) sẽ hiển thị đường dẫn của thư mục đang truy cập tới. Không có gì xảy ra khi thay đổi nội dung thanh địa chỉ.
2. Bấm vào nút Back kế bên thanh địa chỉ sẽ trỏ tới đường dẫn đã truy cập trước đó (thông qua ListView hoặc TreeView), đồng thời hiển thị tập tin và thư mục con của đường dẫn tương ứng.
Nếu trước đó không truy cập đường dẫn nào, khi bấm nút Back sẽ không có gì xảy ra
3. Bấm vào nút Next kế bên thanh địa chỉ sẽ trỏ tới đường dẫn truy cập trước khi bấm nút Back hoặc Up và hiển thị tập tin, thư mục con tương ứng trong ListView.
Nếu đã trở về hết số lần bấm nút Back/Up (hoặc chưa bấm Back/Up lần nào), khi bấm nút Next sẽ không có gì xảy ra.
4. Bấm vào nút Up sẽ trỏ tới địa chỉ đường dẫn thu mục cha (parrent) của thư mục đang truy cập tới, hiển thị tập tin, thư mục con của thư mục cha trong ListView.
5. Bấm chọn 4 tùy chọn trong menu View (Medium Icons, Small Icons, List, Detail) sẽ thay đổi hiển thị ListView theo dạng tương ứng (mặc định khi mở chương trình là Medium Icons).

## Link Repository
<https://bitbucket.org/3ar0n/fileexplorer.git>

## Link Youtube
<https://youtu.be/lr72K9NiAWQ>